'''
Python wrapper for the Tobi Clearview TCP/IP API

written by Hubert F.J.M. Voogd (MSc.), May 2022
Contact Hubert: hubert.voogd@ru.nl
'''


import socket


#(* Default ip address and port number	*)
LOCALHOST_IPADDRESS	       = b'127.0.0.1'
CLEARVIEW_SERVER_PORT_NUMBER  = 4456
TSG_TOBII_RECORDER_IPADDRESS = b"169.254.137.64"


# Type field constants
CV_TRANSACTION_TYPE_QUESTION = b'\x00\x00\x00Q'
CV_TRANSACTION_TYPE_RESPONSE = b'\x00\x00\x00R'


# Command field constants
intcode= 101
CV_CMD_START = intcode.to_bytes(4, 'big')
intcode= 102
CV_CMD_STOP = intcode.to_bytes(4, 'big')
intcode= 103
CV_CMD_LOG = intcode.to_bytes(4, 'big')
#intcode= 104
#CV_CMD_GENERIC_EVENT = intcode.to_bytes(4, 'big')       #  we do not use this command.
#intcode= 105
#CV_CMD_START_USING_NAME = intcode.to_bytes(4, 'big')    #  Old command from Tobii ClearView. Not supported anymore in Tobii Studio


# Errorcode field constants
intcode = 0
CV_PROTOCOLL_ERR_NO_ERR = intcode.to_bytes(4, 'big')
intcode= 0x20000801
CV_PROTOCOLL_ERR_NO_OPERATION_IMPOSSIBLE_IN_CURRENT_STATE = intcode.to_bytes(4, 'big')
intcode= 0x20000802
CV_PROTOCOLL_ERR_NO_NOCALIBSET =  intcode.to_bytes(4, 'big')
intcode= 0x20000803
CV_PROTOCOLL_ERR_NO_BAD_FORMAT =  intcode.to_bytes(4, 'big')
intcode= 0x20000804
CV_PROTOCOLL_ERR_NO_INTERNAL =  intcode.to_bytes(4, 'big')
intcode= 0x20000805
CV_PROTOCOLL_ERR_NO_FILE_EXISTS =  intcode.to_bytes(4, 'big')
intcode= 0x20000806
CV_PROTOCOLL_ERR_NO_FILE_NAME_FORMAT =  intcode.to_bytes(4, 'big')


#(* Max lenght of null terminated array of AnsiChars returned by CV_GetLastError.		*)
MAX_LEN_ERROR_STRING	       = 255

errorcode = CV_PROTOCOLL_ERR_NO_ERR
transaction_id = 0
mysocket = None


def sendCommand(s):
    global errorcode
    global transaction_id
    global mysocket
    transaction_id = transaction_id + 1
    stringToSend = CV_TRANSACTION_TYPE_QUESTION + transaction_id.to_bytes(4, 'big') + transaction_id.to_bytes(4, 'big') + s
    print(stringToSend)
    if mysocket is not None:
        mysocket.send(stringToSend)
        errorcode = mysocket.recv(24)[16:20]
        pass


def startCommand():
    return CV_CMD_START + CV_PROTOCOLL_ERR_NO_ERR + CV_PROTOCOLL_ERR_NO_ERR


def stopCommand():
    return CV_CMD_STOP +  CV_PROTOCOLL_ERR_NO_ERR + CV_PROTOCOLL_ERR_NO_ERR


def logCommand(s):
    print(len(s))
    return CV_CMD_LOG + CV_PROTOCOLL_ERR_NO_ERR + len(s).to_bytes(4, 'big') + bytes(s)


def CV_Init(pIPaddress = TSG_TOBII_RECORDER_IPADDRESS, portnumber = CLEARVIEW_SERVER_PORT_NUMBER, pDebugLogFile = None):
    global mysocket
    mysocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    mysocket.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
    mysocket.connect((pIPaddress, portnumber))
    #return TobiiLib.CV_Init(pIPaddress, portnumber, pDebugLogFile)


def CV_Close():
    global mysocket
    if mysocket is not None:
        mysocket.close()
        mysocket = None


def CV_Start():
    return sendCommand(startCommand())


#def CV_StartWithName(pRecordingName):
#    return TobiiLib.CV_StartWithName(pRecordingName)


def CV_Stop():
    return sendCommand(stopCommand())


def CV_LogEvent(pEventText):
    return sendCommand(logCommand(pEventText))


#def CV_SendGenericEvent():
#    return TobiiLib.CV_SendGenericEvent()


#def CV_GetLastError():
#    return TobiiLib.CV_GetLastError()


def CV_GetLastErrorAsText():
    global errorcode
    if errorcode == CV_PROTOCOLL_ERR_NO_ERR:
        return 'OK. No error.'
    elif errorcode == CV_PROTOCOLL_ERR_NO_OPERATION_IMPOSSIBLE_IN_CURRENT_STATE:
        return 'The command sent is not applicable in the current state of Tobii Studio/Clearview.'
    elif errorcode == CV_PROTOCOLL_ERR_NO_NOCALIBSET:
        return 'There is no calibration set.'
    elif errorcode == CV_PROTOCOLL_ERR_NO_BAD_FORMAT:
        return 'The message received is not understood.'
    elif errorcode == CV_PROTOCOLL_ERR_NO_INTERNAL:
        return 'Internal error of "This should not happen" kind. Please contact support..'
    elif errorcode == CV_PROTOCOLL_ERR_NO_FILE_EXISTS:
        return 'There is another recording already with this name.'
    elif errorcode == CV_PROTOCOLL_ERR_NO_FILE_NAME_FORMAT:
        return 'At least one token in the file name is not allowed by the operating system that Tobii Studio/Clearview runs on..'
    else:
        return 'Unknow error.'


#CV_Init()
#CV_Start()
#CV_Stop()
#CV_LogEvent(b'dit is een event')
#print(CV_GetLastErrorAsText())