'''
Python wrapper for the Tobi Clearview Trigger API

written by Hubert F.J.M. Voogd (MSc.), March 2018
Contact Hubert: h.voogd@socsci.ru.nl
'''
#from ctypes import *
import ctypes
TobiiLib = ctypes.windll.LoadLibrary("cv.dll")


CV_ERROR		       = -1			#(* Was error	*)
CV_NO_ERROR		       =  0			#(* Was no error	*)


#(* These are possible error codes returned by CV_GetLastError.	*)
CV_ERR_NO_ERROR	            = 0
CV_ERR_INVALID_STATE	    = 1
CV_ERR_SERVER_COMMUNICATION = 2
CV_ERR_INTERNAL	            = 3
CV_ERR_MEMORY		        = 4
CV_ERR_BAD_PARAMETER	    = 5
CV_ERR_FILE_EXISTS	        = 6
CV_ERR_BAD_FILENAME	        = 7


#(* Default ip address and port number	*)
LOCALHOST_IPADDRESS	       = b'127.0.0.1'
CLEARVIEW_SERVER_PORT_NUMBER  = 4456
TSG_TOBII_RECORDER_IPADDRESS = b"169.254.137.64"


#(* Max lenght of null terminated array of AnsiChars returned by CV_GetLastError.		*)
MAX_LEN_ERROR_STRING	       = 255


def CV_Init(pIPaddress = TSG_TOBII_RECORDER_IPADDRESS, portnumber = CLEARVIEW_SERVER_PORT_NUMBER, pDebugLogFile = None):
    return TobiiLib.CV_Init(pIPaddress, portnumber, pDebugLogFile)


def CV_Close():
    return TobiiLib.CV_Close()


def CV_Start():
    return TobiiLib.CV_Start()


def CV_StartWithName(pRecordingName):
    return TobiiLib.CV_StartWithName(pRecordingName)


def CV_Stop():
    return TobiiLib.CV_Stop()


def CV_LogEvent(pEventText):
    return TobiiLib.CV_LogEvent(pEventText)


def CV_SendGenericEvent():
    return TobiiLib.CV_SendGenericEvent()


def CV_GetLastError():
    return TobiiLib.CV_GetLastError()


def CV_GetLastErrorAsText():
    #ret = b""   #  c_char_p("")
    ret = ctypes.create_string_buffer(b"\0"*MAX_LEN_ERROR_STRING)
    TobiiLib.CV_GetLastErrorAsText(ret)
    s1 = bytes(ret)
    s = b''
    i = 0
    while ret[i] != b'\0':
        s = s + ret[i]
        i = i + 1
    return s


#print(CV_Init())
#print(CV_GetLastErrorAsText())
