% Matlab wrapper for the Tobi Clearview TCP/IP API
% 
% written by and P.L.C. van den Broek, May 2022 based on python version written by Hubert F.J.M. Voogd (MSc.)
% Contact Hubert: hubert.voogd@ru.nl; 
%      or Philip: philip.vandenbroek@ru.nl
    
classdef TobiiTCPIP < handle
    
    properties (Constant)
        % Default ip address and port number
        LOCALHOST_IPADDRESS	       = '127.0.0.1';
        CLEARVIEW_SERVER_PORT_NUMBER  = 4456;
        TSG_TOBII_RECORDER_IPADDRESS = '169.254.137.64';

        % Type field constants
        CV_TRANSACTION_TYPE_QUESTION = [0, 0, 0, uint8('Q')]; %b'\x00\x00\x00Q';
        CV_TRANSACTION_TYPE_RESPONSE = [0, 0, 0, uint8('R')]; %b'\x00\x00\x00R';

        % Command field constants
        CV_CMD_START = bigEndianBytes(101);
        CV_CMD_STOP = bigEndianBytes(102);
        CV_CMD_LOG = bigEndianBytes(103);
        %CV_CMD_GENERIC_EVENT = bigEndianBytes(104);       %  we do not use this command.
        %CV_CMD_START_USING_NAME = bigEndianBytes(105);    %  Old command from Tobii ClearView. Not supported anymore in Tobii Studio


        % Errorcode field constants
        CV_PROTOCOLL_ERR_NO_ERR = bigEndianBytes(0);
        CV_PROTOCOLL_ERR_NO_OPERATION_IMPOSSIBLE_IN_CURRENT_STATE = bigEndianBytes('0x20000801');
        CV_PROTOCOLL_ERR_NO_NOCALIBSET =  bigEndianBytes('0x20000802');
        CV_PROTOCOLL_ERR_NO_BAD_FORMAT =  bigEndianBytes('0x20000803');
        CV_PROTOCOLL_ERR_NO_INTERNAL =  bigEndianBytes('0x20000804');
        CV_PROTOCOLL_ERR_NO_FILE_EXISTS =  bigEndianBytes('0x20000805');
        CV_PROTOCOLL_ERR_NO_FILE_NAME_FORMAT =  bigEndianBytes('0x20000806');

        % Max lenght of null terminated array of AnsiChars returned by CV_GetLastError.
        MAX_LEN_ERROR_STRING = 255;
    end
    
    properties
        errorcode = TobiiTCPIP.CV_PROTOCOLL_ERR_NO_ERR;
        transaction_id = 0;
        mysocket = [];
    end
    
    methods   
        
        function ret = sendCommand(obj, s)
            obj.transaction_id = obj.transaction_id + 1;
            stringToSend = [obj.CV_TRANSACTION_TYPE_QUESTION, bigEndianBytes(obj.transaction_id), bigEndianBytes(obj.transaction_id), uint8(s)];
            disp(stringToSend)
            if ~isempty(obj.mysocket)
                write(obj.mysocket, stringToSend, "uint8");
                answer = read(obj.mysocket, 24, "uint8");
                obj.errorcode = answer(17:20);
            end
            ret = stringToSend;
        end
            
        function ret = startCommand(obj)
            ret =  [obj.CV_CMD_START, obj.CV_PROTOCOLL_ERR_NO_ERR, obj.CV_PROTOCOLL_ERR_NO_ERR];
        end

        function ret = stopCommand(obj)
            ret = [obj.CV_CMD_STOP, obj.CV_PROTOCOLL_ERR_NO_ERR, obj.CV_PROTOCOLL_ERR_NO_ERR];
        end


        function ret = logCommand(obj, s)
            disp(length(s));
            ret = [obj.CV_CMD_LOG, obj.CV_PROTOCOLL_ERR_NO_ERR, bigEndianBytes(length(s)), uint8(s)];
        end

        function obj = CV_Init(obj, pIPaddress, portnumber, pDebugLogFile)
            if nargin < 2, pIPaddress = obj.TSG_TOBII_RECORDER_IPADDRESS; end
            if nargin < 3, portnumber = obj.CLEARVIEW_SERVER_PORT_NUMBER; end
            if nargin < 4, pDebugLogFile = []; end
            
            disp('Connecting to Tobii server...');
            obj.mysocket = tcpclient(pIPaddress,portnumber, "EnableTransferDelay",false, "Timeout",10, "ConnectTimeout",30);
            disp('Connected!');
            % ??? return TobiiLib.CV_Init(pIPaddress, portnumber, pDebugLogFile)
        end

        function CV_Close(obj)
            if ~isempty(obj.mysocket)
                disp('Cleanup TobiiTCPIP: close tcp/ip socket');
                clear obj.mysocket;
                obj.mysocket = [];
            else
                disp('Cleanup TobiiTCPIP: no tcp/ip socket initialized');
            end
        end

        function ret = CV_Start(obj)
            ret = obj.sendCommand(obj.startCommand());
        end

%        function ret = CV_StartWithName(obj, pRecordingName)
%           ret = TobiiLib.CV_StartWithName(pRecordingName); %NB: python lib!!
%        end

        function ret = CV_Stop(obj)
            ret = obj.sendCommand(obj.stopCommand());
        end

        function ret = CV_LogEvent(obj, pEventText)
            ret = obj.sendCommand(obj.logCommand(pEventText));
        end

%       function ret = CV_SendGenericEvent(obj)
%           ret = TobiiLib.CV_SendGenericEvent(); %NB: python lib!!
%       end

%       function ret = CV_GetLastError(obj)
%           ret = TobiiLib.CV_GetLastError(); %NB: python lib!!
%       end

        function ret = CV_GetLastErrorAsText(obj)
            if obj.errorcode == obj.CV_PROTOCOLL_ERR_NO_ERR
                ret = 'OK. No error.';
            elseif obj.errorcode == obj.CV_PROTOCOLL_ERR_NO_OPERATION_IMPOSSIBLE_IN_CURRENT_STATE
                ret = 'The command sent is not applicable in the current state of Tobii Studio/Clearview.';
            elseif obj.errorcode == obj.CV_PROTOCOLL_ERR_NO_NOCALIBSET
                ret = 'There is no calibration set.';
            elseif obj.errorcode == obj.CV_PROTOCOLL_ERR_NO_BAD_FORMAT
                ret = 'The message received is not understood.';
            elseif obj.errorcode == obj.CV_PROTOCOLL_ERR_NO_INTERNAL
                ret = 'Internal error of "This should not happen" kind. Please contact support..';
            elseif obj.errorcode == obj.CV_PROTOCOLL_ERR_NO_FILE_EXISTS
                ret = 'There is another recording already with this name.';
            elseif obj.errorcode == obj.CV_PROTOCOLL_ERR_NO_FILE_NAME_FORMAT
                ret = 'At least one token in the file name is not allowed by the operating system that Tobii Studio/Clearview runs on..';
            else
                ret = 'Unknow error.';
            end
        end
        
        function delete(obj)
            obj.CV_Close();
        end
    end
end

function ret = bigEndianBytes(number)
    % https://www.mathworks.com/matlabcentral/mlc-downloads/downloads/submissions/17569/versions/1/previews/ByteConversion.html
    if ischar(number), number = hex2dec(number); end
    number = uint32(number);
    if isLittleEndian(), number = swapbytes(number); end
    ret = typecast(number, 'uint8');
end

function ret = isLittleEndian()
    [~, ~, endian] = computer;
    ret = (endian == 'L');
end        


%tobii=TobiiTCPIP
%tobii.CV_Init('169.254.137.64', 4456);