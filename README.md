This repository contains
-cv.dll, the tobii clearview triger library.
-TobiiClearviewTriggerAPI.py, a Python3 wrapper for the library.

Usage:

import TobiiClearviewTriggerAPI as tobii

at the beginning of the script:

tobii.CV_Init()
or
tobii.CV_Init(b'<ipaddress of tobii computer>')  # example: tobii.CV_Init(b'169.254.137.64') # Note the b at the beginning of the string!!

to start the recording:
tobii.CV_Start()

to send a message into your eyedata:
tobii.CV_LogEvent(b'my log event') #Note the b before the beginning of the string.

to stop the recording:
tobii.CV_Stop()

Every function should return 0 to indicate success. And returns -1 if there is an error.
You can get the error number by calling:
tobii.CV_GetLastError()

tobii.CV_GetLastErrorAsText() does work at the moment.

